// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			index.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var page = require('page');

require('./home');
require('./curriculum');
require('./blog');
require('./photos');
require('./videos');
require('./shop');
require('./contact');

page();