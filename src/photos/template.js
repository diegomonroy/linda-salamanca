// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			template.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var yo = require('yo-yo');
var layout = require('../layout');

// Template

var photos = yo`
	<h1>Fotos</h1>
`;

module.exports = layout(photos);