// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			index.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var page = require('page');
var empty = require('empty-element');
var template = require('./template');
var title = require('title');

// Index

page('/inicio', function (ctx, next) {
	title('Linda Salamanca - Inicio');
	var main = document.getElementById('main-container');
	empty(main).appendChild(template);
});