// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			template.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var yo = require('yo-yo');
var layout = require('../layout');

// Template

var blog = yo`
	<h1>Blog</h1>
`;

module.exports = layout(blog);