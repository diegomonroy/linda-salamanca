// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			index.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var yo = require('yo-yo');

// Layout

module.exports = function layout(component) {
	return yo`
		<div class="component">
			${component}
		</div>
	`;
}