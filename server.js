// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			server.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var express = require('express');
var app = express();

// Express

app.set('view engine', 'pug');
app.use(express.static('build'));

app.get('/', function (req, res) {
	res.render('in_development', { title: 'Linda Salamanca' });
});

app.get('/inicio', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Inicio' });
});

app.get('/curriculum', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Currículum' });
});

app.get('/blog', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Blog' });
});

app.get('/fotos', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Fotos' });
});

app.get('/videos', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Videos' });
});

app.get('/tienda', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Tienda' });
});

app.get('/contacto', function (req, res) {
	res.render('index', { title: 'Linda Salamanca - Contacto' });
});

app.listen(8080, function () {
	console.log('Linda Salamanca listening on port 8080!');
});