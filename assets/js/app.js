// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			app.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {

	/* Vegas */

	if ( location.pathname == '/' ) {
		jQuery( 'body' ).vegas({
			preloadImage: true,
			overlay: '/bower_components/vegas/dist/overlays/08.png',
			delay: 8000,
			slides: [
				{ src: 'under-construction-01.jpg', animation: 'kenburns' },
				{ src: 'under-construction-02.jpg', animation: 'kenburnsDown' },
				{ src: 'under-construction-03.jpg', animation: 'kenburns' }
			]
		});
	}

});