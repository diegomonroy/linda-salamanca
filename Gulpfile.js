// JavaScript Document

/* ************************************************************************************************************************

Linda Salamanca

File:			Gulpfile.js
Author:			DM Software
Homepage:		www.dmsoftware.co
Copyright:		2017

************************************************************************************************************************ */

// Requires

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-uglify');
var image = require('gulp-imagemin');
var watch = require('gulp-watch');
var babel = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var watchify = require('watchify');

// CSS

gulp.task('css', function () {
	return gulp
		.src('assets/css/app.scss')
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(rename('app.css'))
		.pipe(gulp.dest('build'));
});

// JS

gulp.task('js', function () {
	return gulp
		.src('assets/js/app.js')
		.pipe(minifyJS())
		.pipe(rename('app.js'))
		.pipe(gulp.dest('build'));
});

// Image

gulp.task('image', function () {
	return gulp
		.src('assets/images/*')
		.pipe(image())
		.pipe(gulp.dest('build'));
});

// Watch

// gulp.task('watch', function () {
// 	gulp.watch(['assets/css/**/*.scss'], ['css']);
// 	gulp.watch(['assets/js/**/*.js'], ['js']);
// });

// Compile

function compile(watch) {
	var bundle = watchify(browserify('./src/index.js'));
	function rebundle() {
		bundle
			.transform(babel, {presets: ['es2015']})
			.bundle()
			.on('error', function (err) {
				console.log(err);
				this.emit('end')
			})
			.pipe(source('index.js'))
			.pipe(rename('index.js'))
			.pipe(gulp.dest('build'));
	}
	if (watch) {
		bundle
			.on('update', function () {
				console.log('--> Bundling...');
				rebundle();
			});
	}
	rebundle();
}

gulp.task('build', function () {
	return compile();
});

gulp.task('watch', function () {
	return compile(true);
});

// Default

gulp.task('default', ['css', 'js', 'image', 'build']);